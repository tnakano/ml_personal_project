# Personal projects - Data Science and AI

This is a repository for some personal projects on Data Science and AI.

```Datacamp_projects``` contains projects in the frame of the online course *DataCamp*.

```Classification_land_cover``` is a classification problem of land cover pictures by satellite.

```R_exercise.ipynb``` is some exercise in R.
